#include <chrono>
#include <fstream>
#include <sstream>
#include <string>
#include <unordered_map>

using namespace std;

struct entry {
	int max, mean, min;
};

int main() {
	wifstream                     source("records.tsv");
	unordered_map<wstring, entry> data;

	int     year, month, day;
	wstring time, title;
	int     listeners, faves;
	wstring last_played;

	while(source) {
		source >> year >> month >> day >> time >> title >> listeners >> faves >>
		  last_played;
		wstringstream ss;
		ss << year << month << day;
		wstring key = ss.str();

		auto it = data.find(key);
		if(it != data.end()) {
			entry& second = it->second;
			if(listeners < second.min)
				second.min = listeners;
			else if(listeners > second.max)
				second.max = listeners;
			// TODO: Min can be 0, it's dumb. Instead record a sum and the number of entires, then compute the means after
		} else {
			data.insert({key, {listeners, listeners, listeners}});
		}
	}
}
