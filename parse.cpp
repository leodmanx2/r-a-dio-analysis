#include <filesystem>
#include <fstream>
#include <iostream>
#include <regex>
#include <sstream>
#include <string>

using namespace std;
namespace fs = std::filesystem;

int main() {
	wofstream out_file(L"records.tsv");
	wstring   current_dj;
	wsmatch   match;

	const wregex file_pattern(
	  L"^dio\\.rizon_(\\d{4})\\.(\\d{2})\\.(\\d{2}).txt$");

	const wregex hanyuu_pattern(
	  L"^\\[(\\d{2}:\\d{2}:\\d{2})\\] <&Hanyuu-sama> Now starting: '(.*)' "
	  L"\\[\\d{2}:\\d{2}\\]\\((\\d+) listeners\\), (\\d+) faves, played (\\d+) "
	  L"times, LP: (.*)$");
	out_file << L"Year\tMonth\tDay\tTime\tTitle\tListeners\tFaves\tTimes "
	            L"played\tLast played\n";

	const wregex dj_pattern(
	  L"^\\[(\\d{2}):(\\d{2}):(\\d{2})\\] .* Stream: (UP|DOWN) DJ: (.*)  .*$");

	int total_files = 0;
	for(auto& directory_iterator : fs::directory_iterator(".")) {
		if(directory_iterator.is_regular_file()) ++total_files;
	}

	int files_completed = 0;
	for(auto& directory_iterator : fs::directory_iterator(".")) {
		if(!directory_iterator.is_regular_file()) continue;
		// Get date
		const wstring filename = directory_iterator.path().filename().wstring();
		if(!regex_match(filename, match, file_pattern)) { continue; }
		const wstring year  = match[1].str();
		const wstring month = match[2].str();
		const wstring day   = match[3].str();

		// Parse file
		wifstream file(directory_iterator.path());
		wstring   line;
		while(getline(file, line)) {
			if(regex_match(line, match, hanyuu_pattern)) {
				out_file << current_dj << L"\t" << year << L"\t" << month << L"\t"
				         << day << L"\t";
				for(auto sub_match = next(match.begin()); sub_match != match.end();
				    ++sub_match) {
					out_file << sub_match->str() << L"\t";
				}
				out_file << L"\n";
			} else if(regex_match(line, match, dj_pattern)) {
				// You have other info you can collect here
				// A disconnect is always followed by a connect and topic message, so
				// they do not need to be checked for
				current_dj = match[5].str();
			}
		}
		cout << '[' << ++files_completed << '/' << total_files << "]          \r";
	}
	cout << '\n';
}
