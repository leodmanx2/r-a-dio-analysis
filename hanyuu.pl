:- [library(dcg/basics)].

filename(Day, Month, Year) --> "dio.rizon_", integer(Year), ".", integer(Month), ".", integer(Day), ".txt".
time(Hours, Minutes, Seconds) --> "[", integer(Hours), ":", integer(Minutes), ":", integer(Seconds), "]".
record(Hours, Minutes, Seconds, Listeners) --> time(Hours, Minutes, Seconds), " <&Hanyuu-sama>", string(_), "](", integer(Listeners), remainder(_).

% TODO: See if you can break up data by DJ, or if the logs are too inconsistent

main :- expand_file_name('*', Files),
        process_files(Files),
        halt.

process_files([]).
process_files([H|T]) :- process(H), process_files(T).

process(File) :- atom_codes(File, Codes),
                 phrase(filename(Day, Month, Year), Codes),
                 open(File, read, Stream),
                 process_lines(Stream, Day, Month, Year),
                 close(Stream),
                 !.
process(_).

process_lines(Stream, _, _, _) :- at_end_of_stream(Stream), !.
process_lines(Stream, Day, Month, Year) :- read_line_to_codes(Stream, Codes),
                                           print_record(Codes, Day, Month, Year),
                                           process_lines(Stream, Day, Month, Year).

print_record(Codes, Day, Month, Year) :- phrase(record(Hour, Minute, Second, Listeners), Codes),
                                         date_time_stamp(date(Year, Month, Day, Hour, Minute, Second, 18000, -, true), Timestamp),
                                         format('~w,~w', [Timestamp, Listeners]),
                                         nl.
print_record(_, _, _, _).
