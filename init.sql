CREATE TABLE Records (
DJ varchar NOT NULL,
YEAR int NOT NULL,
MONTH int(12) NOT NULL,
DAY int(31) NOT NULL,
PLAY_TIME varchar NOT NULL,
TITLE varchar NOT NULL,
LISTENERS int NOT NULL,
FAVES int NOT NULL,
TIMES_PLAYED int NOT NULL,
LAST_PLAY varchar NOT NULL,
PRIMARY KEY (YEAR, MONTH, DAY, PLAY_TIME)
);
